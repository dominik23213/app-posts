export class Comment {
    private _postId: number;
    private _id: number;
    private _name: string;
    private _email: string;
    private _body: string;

    constructor( data : any = null ) {
        if(data) {
            this.nomalizer(data);
        }
    }

    public nomalizer(data) {
        for(const key in data) {
            this[key] = data[key];
        }
    }

    public get id() : number {
        return this._id;
    }

    public set id(v : number) {
        this._id = v;
    }    

    public get name() : string {
        return this._name;
    }

    public set name(v : string) {
        this._name = v;
    }
    
    public get email() : string {
        return this._email;
    }
    
    public set email(v : string) {
        this._email = v;
    }

    public get body() : string {
        return this.body;
    }
    
    public set body(v : string) {
        this._body = v;
    }
}