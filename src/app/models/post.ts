import { Author } from "./author";
import { RestApiService } from "../sercives/rest-api.service";
import { AUTO_STYLE } from "@angular/core";

export class Post {
    private _id: number;
    private _userId: number;
    private _title: string;
    private _body: string;
    private _author: Author = new Author();
    
    constructor( data : any = null ) {
        if(data) {
            this.nomalizer(data);
        }
    }

    public nomalizer(data) {
        for(const key in data) {
            this[key] = data[key];
        }
    }
    
    public get id() : number {
        return this._id
    }

    public set id(v : number) {
        this._id = v;
    }
    
    public get userId() : number {
        return this._userId;
    }

    
    public set userId(v : number) {
        this._userId = v;
    }
    
    
    public get title() : string {
        return this._title;
    }

    public set title(v : string) {
        this._title = v;
    }
    
    public get body() : string {
        return this._body;
    }
    
    public set body(v : string) {
        this._body = v;
    }
    
    public get author() : Author {
        return this._author;
    }
    
    public set author(v : Author) {
        this._author = v;
    }

    public getAuthor(restApiService: RestApiService) {
        restApiService.getUsersData(this.userId).subscribe( (data : any) => {
            this.author = new Author(data);
        })
    }
    
    
}