
export class Author {
    private _id: number;
    private _name: string;
    private _userName: string;
    private _email: string;

    constructor( data : any = null ) {
        if(data) {
            this.nomalizer(data);
        }
    }

    public nomalizer(data) {
        for(const key in data) {
            this[key] = data[key];
        }
    }

    public get id() : number {
        return this._id;
    }

    public set id(v : number) {
        this._id = v;
    }    

    public get name() : string {
        return this._name;
    }

    public set name(v : string) {
        this._name = v;
    }
    
    public get userName() : string {
        return this._userName;
    }
    
    public set userName(v : string) {
        this._userName = v;
    }

    public get email() : string {
        return this.email;
    }
    
    public set email(v : string) {
        this._email = v;
    }
    
}