import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'posts',
		pathMatch: 'full'
	},
	{
		path: 'posts',
		loadChildren: 'app/pages/posts/posts.module#PostsModule' 
	},
	{
		path: 'users',
		loadChildren: 'app/pages/users/users.module#UsersModule'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
