import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RestApiService {
    private _apiUrl = 'https://jsonplaceholder.typicode.com';

    constructor(private _http: HttpClient) {

    }

    getPostsData(idPost = null) {
		let url = this._apiUrl + '/posts';
		
		if(idPost) {
			url += '/' + idPost;
		}

        return this._http.get(url);
	}

	getPostsCommentsData(idPost) {
		let url = this._apiUrl + '/posts';
		
		if(idPost) {
			url += '/' + idPost + '/comments';
		}

        return this._http.get(url);
	}

	getUsersData(idUser = null){
		let url = this._apiUrl + '/users'

		if(idUser) {
			url += '/' + idUser
		}
		return this._http.get(url);
	}
	
}