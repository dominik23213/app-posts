import { Component, OnInit } from '@angular/core';
import { Post } from '../../../models/post';
import { RestApiService } from '../../../sercives/rest-api.service';
import { Author } from '../../../models/author';

@Component({
	selector: 'app-list-posts',
	templateUrl: './list-posts.component.html',
	styleUrls: ['./list-posts.component.css']
})
export class ListPostsComponent implements OnInit {
	public posts: Array<Post> = [];
	public isLoading: boolean;

  	constructor(private restApiService:RestApiService) {
		this.isLoading = true;
		this.restApiService.getPostsData().subscribe((data : any) => {
			if(data instanceof Array) {
				data.forEach((item) => {
					let post = new Post(item);
					post.getAuthor(this.restApiService);
					this.posts.push(post);
				});
				this.isLoading = false;
			}
		});


	}

	ngOnInit() {
	}



}
