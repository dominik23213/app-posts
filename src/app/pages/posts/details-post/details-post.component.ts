import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../../../sercives/rest-api.service';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../../../models/post';
import { Comment } from '../../../models/comment';

@Component({
	selector: 'app-details-post',
	templateUrl: './details-post.component.html',
	styleUrls: ['./details-post.component.css']
})
export class DetailsPostComponent implements OnInit {
	public post: Post;
	public comments: Array<Comment> = [];

	constructor(private restApiService: RestApiService, private route: ActivatedRoute) {
		this.route.params.subscribe(params => {

			this.restApiService.getPostsData(params['id']).subscribe((data: any) => {
				this.post = new Post(data);
				this.post.getAuthor(this.restApiService);
			});

			this.restApiService.getPostsCommentsData(params['id']).subscribe((data: any) => {
				data.forEach(item => {
					this.comments.push(item);
				});
			})

		});
	}

	ngOnInit() {

	}

}
