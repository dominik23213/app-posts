import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { ListPostsComponent } from './list-posts/list-posts.component';
import { DetailsPostComponent } from './details-post/details-post.component';

@NgModule({
  imports: [
    CommonModule,
    PostsRoutingModule
  ],
  declarations: [ListPostsComponent, DetailsPostComponent]
})
export class PostsModule { }
