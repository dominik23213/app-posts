import { Component, OnInit } from '@angular/core';
import { Author } from '../../../models/author';
import { RestApiService } from '../../../sercives/rest-api.service';

@Component({
	selector: 'app-list-users',
	templateUrl: './list-users.component.html',
	styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
	public users : Array<Author> = [];
	public isLoading : boolean;

	constructor(private restApiService:RestApiService) {
		this.isLoading = true; 
		this.restApiService.getUsersData().subscribe( (data) => {
			if(data instanceof Array) {
				data.forEach( (item) => {
					let author = new Author(item);
					this.users.push(author);
				});
			}
		});
	 }

	ngOnInit() {
	}

}
